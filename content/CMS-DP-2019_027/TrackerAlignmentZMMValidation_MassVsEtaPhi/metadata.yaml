title: '$Z^{0}$ peak mass as a function of the pseudorapidity $\eta(\mu^{+})$ and the azimuthal angle $\phi(\mu^{+})$ of the positive muon for 2017 End-of-Year reprocessing and Legacy processing CMS data'
caption: 'Distortions of the tracker geometry can lead to a bias in the reconstructed track curvature $\kappa \propto \pm\frac{1}{p_{T}}$. These are investigated using the reconstructed Z$\rightarrow\mu\mu$ mass as a function of the muon direction and separating $\mu^{+}$ and $\mu^{-}$ as the curvature bias has an opposite effect on their $p_{T}$. The invariant mass distribution is fitted to a Breit-Wigner convolved with a Crystal Ball function, thus taking into account the finite track resolution and the radiative tail, for the signal plus an exponential background. The fit range is 75-105 GeV/$c^{2}$ and the $Z^{0}$ width is fixed to the PDG value of 2.495 GeV/$c^{2}$ [1]. Note that this does not show the CMS muon reconstruction and calibration performance. Additional muon momentum calibrations are applied on top of this in physics analyses. 

$Z^{0}$ peak mass as a function of the pseudorapidity $\eta(\mu^{+})$ and the azimuthal angle $\phi(\mu^{+})$ of the positive muon for 2017 End-of-Year reprocessing and Legacy processing of 2017 CMS data. The z-axis range is centered at the peak value corresponding to the fitted mass for all events in the 2017 sample (91.08 GeV$c^{2}$). In the Legacy reprocessing the overall pattern is significantly reduced with respect to End-Of-Year reprocessing of 2017 data. 

[1] CMS Collaboration "Alignment of the CMS tracker with LHC and cosmic ray data" 2014 JINST 9 P06009 doi:10.1088/1748-0221/9/06/P06009'
date: '2019-09-17'
tags:
- Run-2
- Collisions
- Phase-1 Pixel
- Tracker
- Alignment
- pp
- 2017