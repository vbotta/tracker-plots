title: 'Mean track-vertex impact parameter in the transverse plane $d_{xy}$ as a function of track azimuth $\phi$.'
caption: 'The resolution of the reconstructed vertex position is driven by the pixel detector since it is the closest detector to the interaction point and has the best hit resolution. The primary vertex residual method is based on the study of the distance between the track and the vertex, the latter reconstructed without the track under scrutiny (unbiased track-vertex residual). Selection and reconstruction of the events is the following [1]:

$\bullet$ Events used in this analysis are selected online using collision events triggers without specific selections
$\bullet$ The fit of the vertex must have at least 4 degrees of freedom.
$\bullet$ For each of the vertices, the impact parameters are measured for tracks with:
         $\circ$  more than 6 hits in the tracker, of which at least two are in the pixel detector,
         $\circ$  at least one hit in the first layer of the Barrel Pixel or the first disk of the Forward Pixel,
	 $\circ$  $\chi^{2}/ndof$ of the track fit < 5. 
$\bullet$ The vertex position is recalculated excluding the track under scrutiny.
$\bullet$ A deterministic annealing clustering algorithm [2] is used in order to make the method robust against pileup, as in the default reconstruction sequence. 

The distributions of the unbiased track-vertex residuals in the transverse plane, $d_{xy}$ and in the longitudinal direction, $d_{z}$ are studied in bins of track azimuth $\phi$ and pseudo-rapidity $\eta$. Random misalignments of the modules affect only the resolution of the unbiased track-vertex residual, increasing the width of the distributions, but without biasing their mean. Systematic movements of the modules will bias the distributions in a way that depends on the nature and size of the misalignment and of the selected tracks.

Mean track-vertex impact parameter in the transverse plane $d_{xy}$ as a function of track azimuth $\phi$. The impact parameters are obtained by recalculating the vertex position after removing the track being studied from it, and considering the impact parameter of this removed track. The black points show the results with the alignment constants used during 2017 End-Of-Year reprocessing, the red points show the results with the alignment constants as obtained in the Run-2 Legacy alignment procedure. The alignment constants are time-dependent, those used here were obtained for data taken in July 2017. Modulations visible in this distribution with the alignment constants used in the EOY processing are improved with the Legacy tracker alignment. 

[1] CMS Collaboration "Performance of CMS muon reconstruction in pp collision events at sqrt(s) = 7 TeV" JINST 7 (2012) P10002 doi:10.1088/1748-0221/7/10/P10002 
[2] R. Fr\"uhwirth, W. Waltenberger, and P. Vanlaer, "Adaptive Vertex Fitting", CMS NOTE 2007-008, CERN, 2007.'

date: '2019-09-17'
tags:
- Run-2
- Collisions
- Phase-1 Pixel
- Tracker
- Alignment
- pp
- 2017